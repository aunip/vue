import { generateId } from '../utils';
import pizzas from '../pizzas.json';

/**
 * Read All Pizzas
 */
export const readAllPizzas = () => {
  return new Promise(resolve => {
    resolve(
      pizzas.map(pizza => {
        pizza.id = generateId();
        return pizza;
      })
    );
  });
};
