# All U Need Is Pizza

> _Made With **NodeJS** 12_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- public
    +-- favicon.png
    +-- index.html
+-- src
    +-- assets
        +-- fonts
            +-- Nunito-Bold.ttf
            +-- Nunito-Regular.ttf
    +-- components
        +-- layers
            +-- Block.vue
            +-- HyperLink.vue
            +-- index.js
            +-- Radio.vue
            +-- Row.vue
            +-- TextField.vue
        +-- views
            +-- Add.vue
            +-- index.js
            +-- Info.vue
            +-- List.vue
        +-- App.vue
    +-- services
        +-- pizza.js
    +-- utils
        +-- index.js
    +-- index.css
    +-- main.js
    +-- pizzas.json
+-- .browserslistrc
+-- .eslintrc.js
+-- .gitignore
+-- .prettierrc
+-- babel.config.js
+-- jest.config.js
+-- LICENSE
+-- package.json
+-- postcss.config.js
+-- README.md
+-- yarn.lock
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/vue.git
```

Install:

```
npm install
```

Launch:

```
npm run serve
```

Build:

```
npm run build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
